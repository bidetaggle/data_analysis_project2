import numpy as np
import matplotlib.pyplot as plt

## Creating random samples from the initial sample
A=[181, 167, 175, 187, 172, 173, 179, 193, 164, 188]
mean_list=[]
for k in range(50000):
    B=np.random.choice(A,10)
    mean_list.append(np.mean(B))

## Mean and spread of means
simple_mean = np.mean(A);
mean = np.mean(mean_list)
spread = np.subtract(*np.percentile(mean_list, [75, 25]))
print("simple mean : "+str(simple_mean))
print("Mean :"+str(mean))
print("Spread : "+str(spread))

## Plot
plt.hist(mean_list, bins=500)
plt.show()
