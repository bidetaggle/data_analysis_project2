# Report #

## tools ##

I found numpy and matplotlib libraries in python to have mathematical functions and to draw diagram based on arrays. The python executable is bootstrapping.py

## usage ##

`$ sudo pip install numpy`
`$ sudo pip install matplotlib`
`$ python bootstrapping.py`

## explainations ##

I made 50000 resamplings with a loop which are store in mean_list. I also calculated the mean of the initial sample dataset (simple_mean) to compare with the resampling technique and the both results are pretty close in general.

![diagram](diagram.png)

output :
![output bootstrapping.py](output.png)

The spread is around 3.8-3.9 which is pretty big according to the dataset range value of 29 (193 - 164)
